from flask import Flask, request, redirect, render_template
from TwitterData import file_main
app = Flask(__name__)


@app.route('/')
def hello_world():
    return render_template('getURL.html')

@app.route('/testURL', methods=["POST"])
def testingURL():
    hashtag = request.form.get('hashtag', '')
    file_main(hashtag1=hashtag)
    return 'HELLO WORLD'

if __name__ == '__main__':
    app.run(debug=True)
